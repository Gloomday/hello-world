<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 24.04.2017
 * Time: 12:36
 */

namespace frontend\controllers;


use yii\web\Controller;

class HelloController extends Controller
{
    public function actionWorld($message = 'World Hello')
    {
        return $this->render('world', ['message' => $message]);
    }
}